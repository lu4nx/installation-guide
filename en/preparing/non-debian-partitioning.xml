<!-- retain these comments for translator revision tracking -->
<!-- $Id$ -->

 <sect1 id="non-debian-partitioning">
 <title>Pre-Partitioning for Multi-Boot Systems</title>
<para>

Partitioning your disk simply refers to the act of breaking up your
disk into sections. Each section is then independent of the others.
It's roughly equivalent to putting up walls inside a house; if you add
furniture to one room it doesn't affect any other room.

</para><para arch="s390">

Whenever this section talks about <quote>disks</quote> you should translate
this into a DASD or VM minidisk in the &arch-title; world. Also a machine
means an LPAR or VM guest in this case.

</para><para>

If you already have an operating system on your system

<phrase arch="any-x86">
(Windows, OS/2, MacOS, Solaris, FreeBSD, &hellip;)
</phrase>

<phrase arch="s390">
(VM, z/OS, OS/390, &hellip;)
</phrase>

which uses the whole disk and you want to stick &debian; on the same disk, you will need to repartition
it. &debian; requires its own hard disk partitions. It cannot be
installed on Windows or Mac OS X partitions. It may be able to share some
partitions with other Unix systems, but that's not covered here. At
the very least you will need a dedicated partition for the &debian;
root filesystem.

</para><para>

You can find information about your current partition setup by using
a partitioning tool for your current operating system<phrase
arch="any-x86">, such as the integrated Disk Manager in Windows</phrase><phrase
arch="powerpc">, such as Disk Utility, Drive Setup, HD Toolkit, or MacTools</phrase><phrase
arch="s390">, such as the VM diskmap</phrase>. Partitioning tools always
provide a way to show existing partitions without making changes.

</para><para>

In general, changing a partition with a file system already on
it will destroy any information there. Thus you should always make
backups before doing any repartitioning.  Using the analogy of the
house, you would probably want to move all the furniture out of the
way before moving a wall or you risk destroying it.

</para><para>

Several modern operating systems offer the ability to move and resize
certain existing partitions without destroying their contents.  This allows
making space for additional partitions without losing existing data.  Even
though this works quite well in most cases, making changes to the
partitioning of a disk is an inherently dangerous action and should only be
done after having made a full backup of all data.
<phrase arch="any-x86">For FAT/FAT32 and NTFS partitions as used by
Windows systems, the ability to move and resize them losslessly is provided 
both by &d-i; as well as by the integrated Disk Manager of Windows.</phrase>

</para>

<para arch="x86">
To losslessly resize an existing FAT or NTFS partition from within &d-i;, go
to the partitioning step, select the option for manual partitioning, select
the partition to resize, and simply specify its new size.
</para>

<!--
<para arch="powerpc">

In order for OpenFirmware to automatically boot &debian-gnu; the &arch-parttype;
partitions should appear before all other partitions on the disk,
especially Mac OS X boot partitions. This should be kept in mind when
pre-partitioning; you should create a &arch-parttype; placeholder partition to
come <emphasis>before</emphasis> the other bootable partitions on the
disk. (The small partitions dedicated to Apple disk drivers are not
bootable.) You can delete the placeholder with the &debian; partition
tools later during the actual install, and replace it with &arch-parttype;
partitions.

</para>
-->

&nondeb-part-powerpc.xml;

 </sect1>
