# Translation of the Debian installation-guide into Indonesian.
# Andika Triwidada <andika@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: d-i-manual_administrivia\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2022-05-19 22:02+0000\n"
"PO-Revision-Date: 2022-12-30 10:52+0000\n"
"Last-Translator: Andika Triwidada <andika@gmail.com>\n"
"Language-Team: Debian Indonesia Translators <debian-l10n-indonesian@lists."
"debian.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.15.1-dev\n"

#. Tag: title
#: boot-new.xml:5
#, no-c-format
msgid "Booting Into Your New &debian; System"
msgstr "Boot Ke Sistem &debian; Baru Anda"

#. Tag: title
#: boot-new.xml:7
#, no-c-format
msgid "The Moment of Truth"
msgstr "Momen Kebenaran"

#. Tag: para
#: boot-new.xml:8
#, no-c-format
msgid ""
"Your system's first boot on its own power is what electrical engineers call "
"the <quote>smoke test</quote>."
msgstr ""
"Boot pertama sistem Anda dengan kekuatannya sendiri adalah apa yang oleh "
"insinyur kelistrikan disebut <quote>uji asap</quote>."

#. Tag: para
#: boot-new.xml:13
#, no-c-format
msgid ""
"If you did a default installation, the first thing you should see when you "
"boot the system is the menu of the <classname>grub</classname> bootloader. "
"The first choices in the menu will be for your new &debian; system. If you "
"had any other operating systems on your computer (like Windows) that were "
"detected by the installation system, those will be listed lower down in the "
"menu."
msgstr ""
"Jika Anda melakukan instalasi baku, hal pertama yang akan Anda lihat ketika "
"Anda melakukan boot sistem adalah menu bootloader <classname>grub</"
"classname>. Pilihan pertama dalam menu tersebut adalah untuk sistem &debian; "
"baru Anda. Jika Anda memiliki sistem operasi lain di komputer Anda (seperti "
"Windows) yang terdeteksi oleh sistem instalasi, maka sistem operasi tersebut "
"akan terdaftar di bagian bawah menu."

#. Tag: para
#: boot-new.xml:23
#, no-c-format
msgid ""
"If the system fails to start up correctly, don't panic. If the installation "
"was successful, chances are good that there is only a relatively minor "
"problem that is preventing the system from booting &debian;. In most cases "
"such problems can be fixed without having to repeat the installation. One "
"available option to fix boot problems is to use the installer's built-in "
"rescue mode (see <xref linkend=\"rescue\"/>)."
msgstr ""
"Jika sistem gagal memulai dengan benar, jangan panik. Jika instalasi "
"berhasil, kemungkinan besar hanya ada masalah yang relatif kecil yang "
"mencegah sistem melakukan boot &debian;. Dalam kebanyakan kasus, masalah "
"tersebut dapat diperbaiki tanpa harus mengulangi instalasi. Salah satu "
"pilihan yang tersedia untuk memperbaiki masalah boot adalah dengan "
"menggunakan mode penyelamatan bawaan installer (lihat <xref linkend=\"rescue"
"\"/>)."

#. Tag: para
#: boot-new.xml:32
#, no-c-format
msgid ""
"If you are new to &debian; and &arch-kernel;, you may need some help from "
"more experienced users. <phrase arch=\"x86\">For direct on-line help you can "
"try the IRC channels #debian or #debian-boot on the OFTC network. "
"Alternatively you can contact the <ulink url=\"&url-list-subscribe;\">debian-"
"user mailing list</ulink>.</phrase> <phrase arch=\"not-x86\">For less common "
"architectures like &arch-title;, your best option is to ask on the <ulink "
"url=\"&url-list-subscribe;\">debian-&arch-listname; mailing list</ulink>.</"
"phrase> You can also file an installation report as described in <xref "
"linkend=\"submit-bug\"/>. Please make sure that you describe your problem "
"clearly and include any messages that are displayed and may help others to "
"diagnose the issue."
msgstr ""
"Jika Anda baru mengenal &debian; dan &arch-kernel;, Anda mungkin memerlukan "
"bantuan dari pengguna yang lebih berpengalaman. <phrase arch=\"x86\">Untuk "
"bantuan langsung secara daring, Anda dapat mencoba kanal IRC #debian atau "
"#debian-boot pada jaringan OFTC. Atau Anda dapat menghubungi <ulink url="
"\"&url-list-subscribe;\">milis debian-user</ulink>.</phrase> <phrase arch="
"\"not-x86\">Untuk arsitektur yang kurang umum seperti &arch-title;, pilihan "
"terbaik Anda adalah bertanya pada <ulink url=\"&url-list-subscribe;\">milis "
"debian-&arch-listname;</ulink>.</phrase> Anda juga dapat mengajukan laporan "
"instalasi seperti yang dijelaskan di <xref linkend=\"submit-bug\"/>. "
"Pastikan bahwa Anda mendeskripsikan masalah Anda dengan jelas dan "
"menyertakan pesan-pesan yang ditampilkan dan dapat membantu orang lain untuk "
"mendiagnosa masalah tersebut."

#. Tag: para
#: boot-new.xml:48
#, no-c-format
msgid ""
"If you had any other operating systems on your computer that were not "
"detected or not detected correctly, please file an installation report."
msgstr ""
"Jika Anda memiliki sistem operasi lain pada komputer Anda yang tidak "
"terdeteksi atau tidak terdeteksi dengan benar, harap ajukan laporan "
"instalasi."

#. Tag: title
#: boot-new.xml:150
#, no-c-format
msgid "Mounting encrypted volumes"
msgstr "Mengait volume terenkripsi"

#. Tag: para
#: boot-new.xml:152
#, no-c-format
msgid ""
"If you created encrypted volumes during the installation and assigned them "
"mount points, you will be asked to enter the passphrase for each of these "
"volumes during the boot."
msgstr ""
"Jika Anda membuat volume terenkripsi selama instalasi dan menetapkan titik "
"kait, Anda akan diminta untuk memasukkan frasa sandi bagi masing-masing "
"volume ini selama boot."

#. Tag: para
#: boot-new.xml:160
#, no-c-format
msgid ""
"For partitions encrypted using dm-crypt you will be shown the following "
"prompt during the boot: <informalexample><screen>\n"
"Starting early crypto disks... <replaceable>part</"
"replaceable>_crypt(starting)\n"
"Enter LUKS passphrase:\n"
"</screen></informalexample> In the first line of the prompt, "
"<replaceable>part</replaceable> is the name of the underlying partition, e."
"g. sda2 or md0. You are now probably wondering <emphasis>for which volume</"
"emphasis> you are actually entering the passphrase. Does it relate to your "
"<filename>/home</filename>? Or to <filename>/var</filename>? Of course, if "
"you have just one encrypted volume, this is easy and you can just enter the "
"passphrase you used when setting up this volume. If you set up more than one "
"encrypted volume during the installation, the notes you wrote down as the "
"last step in <xref linkend=\"partman-crypto\"/> come in handy. If you did "
"not make a note of the mapping between <filename><replaceable>part</"
"replaceable>_crypt</filename> and the mount points before, you can still "
"find it in <filename>/etc/crypttab</filename> and <filename>/etc/fstab</"
"filename> of your new system."
msgstr ""
"Untuk partisi yang dienkripsi menggunakan dm-crypt Anda akan diperlihatkan "
"prompt berikut selama boot: <informalexample><screen>\n"
"Starting early crypto disks... <replaceable>part</"
"replaceable>_crypt(starting)\n"
"Enter LUKS passphrase:\n"
"</screen></informalexample> Di baris pertama prompt, <replaceable>part</"
"replaceable> adalah nama partisi yang mendasarinya, misalnya sda2 atau md0. "
"Anda sekarang mungkin bertanya-tanya <emphasis>untuk volume mana</emphasis> "
"Anda benar-benar memasukkan frasa sandi. Apakah itu berhubungan dengan "
"<filename>/home</filename> Anda? Atau ke <filename>/var</filename>? Tentu "
"saja, jika Anda hanya memiliki satu volume terenkripsi, ini mudah dan Anda "
"cukup memasukkan frasa sandi yang Anda gunakan saat mengatur volume ini. "
"Jika Anda mengatur lebih dari satu volume terenkripsi selama instalasi, "
"catatan yang Anda tulis sebagai langkah terakhir dalam <xref linkend="
"\"partman-crypto\"/> berguna. Jika Anda tidak mencatat pemetaan antara "
"<filename><replaceable>part</replaceable>_crypt</filename> dan titik kait "
"sebelumnya, Anda masih dapat menemukannya di <filename>/etc/crypttab</"
"filename> dan <filename>/etc/fstab</filename> dari sistem baru Anda."

#. Tag: para
#: boot-new.xml:183
#, no-c-format
msgid ""
"The prompt may look somewhat different when an encrypted root file system is "
"mounted. This depends on which initramfs generator was used to generate the "
"initrd used to boot the system. The example below is for an initrd generated "
"using <classname>initramfs-tools</classname>:"
msgstr ""
"Prompt mungkin terlihat agak berbeda ketika sistem berkas root terenkripsi "
"dipasang. Ini tergantung pada generator initramfs mana yang digunakan untuk "
"menghasilkan initrd yang digunakan untuk mem-boot sistem. Contoh di bawah "
"ini adalah untuk initrd yang dihasilkan menggunakan <classname>initramfs-"
"tools</classname>:"

#. Tag: screen
#: boot-new.xml:190
#, no-c-format
msgid ""
"Begin: Mounting <emphasis>root file system</emphasis>... ...\n"
"Begin: Running /scripts/local-top ...\n"
"Enter LUKS passphrase:"
msgstr ""
"Begin: Mounting <emphasis>root file system</emphasis>... ...\n"
"Begin: Running /scripts/local-top ...\n"
"Enter LUKS passphrase:"

#. Tag: para
#: boot-new.xml:192
#, no-c-format
msgid ""
"No characters (even asterisks) will be shown while entering the passphrase. "
"If you enter the wrong passphrase, you have two more tries to correct it. "
"After the third try the boot process will skip this volume and continue to "
"mount the next filesystem. Please see <xref linkend=\"crypto-troubleshooting"
"\"/> for further information."
msgstr ""
"Tidak ada karakter (bahkan tanda bintang) yang akan ditampilkan saat "
"memasukkan frasa sandi. Jika Anda memasukkan kata sandi yang salah, Anda "
"memiliki dua percobaan lagi untuk memperbaikinya. Setelah percobaan ketiga, "
"proses boot akan melewati volume ini dan terus memasang sistem berkas "
"berikutnya. Silakan lihat <xref linkend=\"crypto-troubleshooting\"/> untuk "
"informasi lebih lanjut."

#. Tag: para
#: boot-new.xml:200
#, no-c-format
msgid "After entering all passphrases the boot should continue as usual."
msgstr ""
"Setelah memasukkan semua frasa sandi, boot mesti berlanjut seperti biasa."

#. Tag: title
#: boot-new.xml:207
#, no-c-format
msgid "Troubleshooting"
msgstr "Penelusuran masalah"

#. Tag: para
#: boot-new.xml:209
#, no-c-format
msgid ""
"If some of the encrypted volumes could not be mounted because a wrong "
"passphrase was entered, you will have to mount them manually after the boot. "
"There are several cases."
msgstr ""
"Jika beberapa volume terenkripsi tidak dapat dikait karena frasa sandi yang "
"salah dimasukkan, Anda harus mengaitnya secara manual setelah boot. Ada "
"beberapa kasus."

#. Tag: para
#: boot-new.xml:218
#, no-c-format
msgid ""
"The first case concerns the root partition. When it is not mounted "
"correctly, the boot process will halt and you will have to reboot the "
"computer to try again."
msgstr ""
"Kasus pertama menyangkut partisi root. Ketika tidak dipasang dengan benar, "
"proses boot akan berhenti dan Anda harus me-reboot komputer untuk mencoba "
"lagi."

#. Tag: para
#: boot-new.xml:225
#, no-c-format
msgid ""
"The easiest case is for encrypted volumes holding data like <filename>/home</"
"filename> or <filename>/srv</filename>. You can simply mount them manually "
"after the boot."
msgstr ""
"Kasus termudah adalah untuk volume terenkripsi yang menyimpan data seperti "
"<filename>/home</filename> atau <filename>/srv</filename>. Anda cukup "
"memasangnya secara manual setelah boot."

#. Tag: para
#: boot-new.xml:231
#, no-c-format
msgid ""
"However for dm-crypt this is a bit tricky. First you need to register the "
"volumes with <application>device mapper</application> by running: "
"<informalexample><screen>\n"
"<prompt>#</prompt> <userinput>/etc/init.d/cryptdisks start</userinput>\n"
"</screen></informalexample> This will scan all volumes mentioned in "
"<filename>/etc/crypttab</filename> and will create appropriate devices under "
"the <filename>/dev</filename> directory after entering the correct "
"passphrases. (Already registered volumes will be skipped, so you can repeat "
"this command several times without worrying.) After successful registration "
"you can simply mount the volumes the usual way:"
msgstr ""
"Namun untuk dm-crypt ini agak rumit. Pertama, Anda perlu mendaftarkan volume "
"dengan <application>device mapper</application> dengan menjalankan: "
"<informalexample><screen>\n"
"<prompt>#</prompt> <userinput>/etc/init.d/cryptdisks start</userinput>\n"
"</screen></informalexample> Ini akan memindai semua volume yang disebutkan "
"dalam <filename>/etc/crypttab</filename> dan akan membuat perangkat yang "
"sesuai di bawah direktori <filename>/dev</filename> setelah memasukkan frasa "
"sandi yang benar. (Volume yang sudah terdaftar akan dilewati, sehingga Anda "
"dapat mengulangi perintah ini beberapa kali tanpa khawatir.) Setelah "
"pendaftaran berhasil, Anda cukup memasang volume dengan cara biasa:"

#. Tag: screen
#: boot-new.xml:246
#, no-c-format
msgid ""
"<prompt>#</prompt> <userinput>mount <replaceable>/mount_point</replaceable></"
"userinput>"
msgstr ""
"<prompt>#</prompt> <userinput>mount <replaceable>/titik_kait</replaceable></"
"userinput>"

#. Tag: para
#: boot-new.xml:249
#, no-c-format
msgid ""
"If any volume holding noncritical system files could not be mounted "
"(<filename>/usr</filename> or <filename>/var</filename>), the system should "
"still boot and you should be able to mount the volumes manually like in the "
"previous case. However, you will also need to (re)start any services usually "
"running in your default runlevel because it is very likely that they were "
"not started. The easiest way is to just reboot the computer."
msgstr ""
"Jika ada volume yang menyimpan berkas sistem non kritis tidak dapat dipasang "
"(<filename>/usr</filename> atau <filename>/var</filename>), sistem harus "
"tetap boot dan Anda harus dapat memasang volume secara manual seperti pada "
"kasus sebelumnya. Namun, Anda juga perlu (memulai kembali) layanan apa pun "
"yang biasanya berjalan di runlevel default Anda karena kemungkinan besar "
"layanan tersebut tidak dimulai. Cara termudah adalah dengan me-reboot "
"komputer."

#. Tag: title
#: boot-new.xml:267
#, no-c-format
msgid "Log In"
msgstr "Log Masuk"

#. Tag: para
#: boot-new.xml:269
#, no-c-format
msgid ""
"Once your system boots, you'll be presented with the login prompt. Log in "
"using the personal login and password you selected during the installation "
"process. Your system is now ready for use."
msgstr ""
"Setelah sistem Anda boot, Anda akan disajikan dengan prompt login. Masuk "
"menggunakan login pribadi dan kata sandi yang Anda pilih selama proses "
"instalasi. Sistem Anda sekarang siap digunakan."

#. Tag: para
#: boot-new.xml:275
#, no-c-format
msgid ""
"If you are a new user, you may want to explore the documentation which is "
"already installed on your system as you start to use it. There are currently "
"several documentation systems, work is proceeding on integrating the "
"different types of documentation. Here are a few starting points."
msgstr ""
"Jika Anda adalah pengguna baru, Anda mungkin ingin menjelajahi dokumentasi "
"yang sudah dipasang pada sistem Anda saat Anda mulai menggunakannya. Saat "
"ini ada beberapa sistem dokumentasi, pekerjaan sedang berlangsung untuk "
"mengintegrasikan berbagai jenis dokumentasi. Berikut adalah beberapa titik "
"awal."

#. Tag: para
#: boot-new.xml:283
#, no-c-format
msgid ""
"Documentation accompanying programs you have installed can be found in "
"<filename>/usr/share/doc/</filename>, under a subdirectory named after the "
"program (or, more precise, the &debian; package that contains the program). "
"However, more extensive documentation is often packaged separately in "
"special documentation packages that are mostly not installed by default. For "
"example, documentation about the package management tool <command>apt</"
"command> can be found in the packages <classname>apt-doc</classname> or "
"<classname>apt-howto</classname>."
msgstr ""
"Dokumentasi yang menyertai program yang telah Anda pasang dapat ditemukan di "
"<filename>/usr/share/doc/</filename>, di bawah sub direktori yang dinamai "
"menurut program (atau, lebih tepatnya, paket &debian; yang berisi program). "
"Namun, dokumentasi yang lebih luas sering dikemas secara terpisah dalam "
"paket dokumentasi khusus yang sebagian besar tidak dipasang secara baku. "
"Misalnya, dokumentasi tentang alat manajemen paket <command>apt</"
"command>dapat ditemukan di paket <classname>apt-doc</classname> atau "
"<classname>apt-howto</classname>."

#. Tag: para
#: boot-new.xml:294
#, no-c-format
msgid ""
"In addition, there are some special folders within the <filename>/usr/share/"
"doc/</filename> hierarchy. Linux HOWTOs are installed in <emphasis>.gz</"
"emphasis> (compressed) format, in <filename>/usr/share/doc/HOWTO/en-txt/</"
"filename>. After installing <classname>dhelp</classname>, you will find a "
"browsable index of documentation in <filename>/usr/share/doc/HTML/index."
"html</filename>."
msgstr ""
"Selain itu, ada beberapa folder khusus dalam hierarki <filename>/usr/share/"
"doc/</filename>. Linux HOWTOs dipasang dalam format <emphasis>.gz</emphasis> "
"(terkompresi), dalam <filename>/usr/share/doc/HOWTO/en-txt/</filename>. "
"Setelah memasang <classname>dhelp</classname>, Anda akan menemukan indeks "
"dokumentasi yang dapat dijelajahi di <filename>/usr/share/doc/HTML/index."
"html</filename>."

#. Tag: para
#: boot-new.xml:303
#, no-c-format
msgid ""
"One easy way to view these documents using a text based browser is to enter "
"the following commands: <informalexample><screen>\n"
"$ cd /usr/share/doc/\n"
"$ w3m .\n"
"</screen></informalexample> The dot after the <command>w3m</command> command "
"tells it to show the contents of the current directory."
msgstr ""
"Salah satu cara mudah untuk melihat dokumen-dokumen ini menggunakan peramban "
"berbasis teks adalah dengan memasukkan perintah berikut: "
"<informalexample><screen>\n"
"$ cd /usr/share/doc/\n"
"$ w3m .\n"
"</screen></informalexample> Titik setelah perintah <command>w3m</command> "
"memberitahunya untuk menampilkan konten direktori saat ini."

#. Tag: para
#: boot-new.xml:313
#, no-c-format
msgid ""
"If you have a graphical desktop environment installed, you can also use its "
"web browser. Start the web browser from the application menu and enter "
"<userinput>/usr/share/doc/</userinput> in the address bar."
msgstr ""
"Jika Anda lingkungan desktop grafis terpasng, Anda juga dapat menggunakan "
"peramban webnya. Mulai peramban web dari menu aplikasi dan masukkan "
"<userinput>/usr/share/doc/</userinput> di bilah alamat."

#. Tag: para
#: boot-new.xml:319
#, no-c-format
msgid ""
"You can also type <userinput>info <replaceable>command</replaceable></"
"userinput> or <userinput>man <replaceable>command</replaceable></userinput> "
"to see documentation on most commands available at the command prompt. "
"Typing <userinput>help</userinput> will display help on shell commands. And "
"typing a command followed by <userinput>--help</userinput> will usually "
"display a short summary of the command's usage. If a command's results "
"scroll past the top of the screen, type <userinput>|&nbsp;more</userinput> "
"after the command to cause the results to pause before scrolling past the "
"top of the screen. To see a list of all commands available which begin with "
"a certain letter, type the letter and then two tabs."
msgstr ""
"Anda juga dapat mengetik <userinput>info <replaceable>perintah</"
"replaceable></userinput> atau <userinput>man <replaceable>command</"
"replaceable></userinput> untuk melihat dokumentasi tentang sebagian besar "
"perintah yang tersedia di prompt. Mengetik <userinput>help</userinput> akan "
"menampilkan bantuan pada perintah shell. Dan mengetik perintah diikuti "
"dengan <userinput>--help</userinput> biasanya akan menampilkan ringkasan "
"singkat dari penggunaan perintah. Jika hasil perintah menggulir melewati "
"bagian atas layar, ketik <userinput>|&nbsp;more</userinput> setelah perintah "
"untuk menyebabkan hasil dijeda sebelum menggulir melewati bagian atas layar. "
"Untuk melihat daftar semua perintah yang tersedia yang dimulai dengan huruf "
"tertentu, ketik huruf dan kemudian dua tab."
